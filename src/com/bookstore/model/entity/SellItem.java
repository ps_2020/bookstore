package com.bookstore.model.entity;

public class SellItem {

    private Long sellItemId;
    private Long sellId;
    private Long bookId;
    private int quantity;

    public SellItem(Long sellId, Long bookId, int quantity) {
        this.sellId = sellId;
        this.bookId = bookId;
        this.quantity = quantity;
    }

    public SellItem(Long sellId, Long bookId, Long sellItemId) {
        this.sellId = sellId;
        this.bookId = bookId;
        this.quantity = quantity;
        this.sellItemId = sellItemId;
    }

    public SellItem() {}

    public Long getSellItemId() {
        return sellItemId;
    }

    public void setSellItemId(Long sellItemId) {
        this.sellItemId = sellItemId;
    }

    public Long getSellId() {
        return sellId;
    }

    public void setSellId(Long sellId) {
        this.sellId = sellId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "SellItem{" +
                "sellItemId=" + sellItemId +
                ", sellId=" + sellId +
                ", bookId=" + bookId +
                ", quantity=" + quantity +
                "}\n";
    }
}
