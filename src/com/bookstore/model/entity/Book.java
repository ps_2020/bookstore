package com.bookstore.model.entity;

public class Book {

    private Long bookId;
    private String title;
    private String genre;
    private String author;
    private double price;
    private int quantity;

    public Book() {}

    public Book(String title, String genre, String author, double price, int quantity) {
        this.title = title;
        this.genre = genre;
        this.author = author;
        this.price = price;
    }

    public Book(String title, String genre, String author, double price, int quantity, Long bookId) {
        this.title = title;
        this.genre = genre;
        this.author = author;
        this.price = price;
        this.bookId = bookId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return  "Book Id: " + bookId +
                ", Title: '" + title + '\'' +
                ", Genre: '" + genre + '\'' +
                ", Author: '" + author + '\'' +
                ", Price: " + price +
                ", Quantity: " + quantity +
                "\n";
    }

    public String toCSV(){
        return       bookId +
                "," + title +
                "," + genre +
                "," + author +
                "," + price +
                "," + quantity +
                "\n";
    }
}
