package com.bookstore.model.entity;

import java.util.Date;

public class Sell {

    private Long sellId;
    private Date date;
    private double totalPrice;
    private Long userId;

    public Sell() {}

    public Sell(Date date, double totalPrice, Long sellId) {
        this.sellId = sellId;
        this.date = date;
        this.totalPrice = totalPrice;
    }

    public Sell(double totalPrice) {
        this.date = new Date();
        this.totalPrice = totalPrice;
    }

    public Sell(double totalPrice, Long sellId) {
        this.date = new Date();
        this.totalPrice = totalPrice;
        this.sellId = sellId;
    }

    public Long getSellId() {
        return sellId;
    }

    public void setSellId(Long sellId) {
        this.sellId = sellId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Sell{" +
                "sellId=" + sellId +
                ", date=" + date +
                ", totalPrice=" + totalPrice +
                "}\n";
    }
}
