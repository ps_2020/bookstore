package com.bookstore.model.dataTransfer;

import com.bookstore.model.entity.Sell;
import com.bookstore.model.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SellDTO {

    private Long sellId;
    private User user;
    private Date date;
    private List<SellItemDTO> sellItems;
    private double totalPrice;

    public SellDTO() {
        this.sellItems = new ArrayList<SellItemDTO>();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getSellId() {
        return sellId;
    }

    public void setSellId(Long sellId) {
        this.sellId = sellId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<SellItemDTO> getSellItems() {
        return sellItems;
    }

    public void setSellItems(List<SellItemDTO> sellItems) {
        this.sellItems = sellItems;
    }

    public void addSellItem(SellItemDTO sellItem){
        if(sellItem.getBook() != null) {
            this.sellItems.add(sellItem);
            this.totalPrice += (sellItem.getQuantity() * sellItem.getBook().getPrice());
        }
    }


    @Override
    public String toString() {
        return "SellDTO{" +
                "sellId=" + sellId +
                ", date=" + date +
                ", sellItems=" + sellItems +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
