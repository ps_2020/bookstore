package com.bookstore.model.repoInterface;

import com.bookstore.model.entity.Book;

import java.util.List;

public interface IBookRepository {
    Book create(Book book);
    Book update(Book book);
    Boolean delete(Long id);
    Book findById(Long id);
    List<Book> findAll();
    List<Book> search(String searchString);
}
