package com.bookstore.model.repoInterface;

import com.bookstore.model.entity.Sell;

import java.util.List;

public interface ISellRepository {
    Sell create(Sell sell);
    Sell update(Sell sell);
    Boolean delete(Long id);
    Sell findById(Long id);
    List<Sell> findAll();
    List<Sell> findAllByUserId(Long id);
}
