package com.bookstore.model.repoInterface;

import com.bookstore.model.entity.User;

import java.util.List;

public interface IUserRepository {
    User create(User User);
    User update(User User);
    Boolean delete(Long id);
    User findById(Long id);
    List<User> findAll();
    User findByUsername(String username);
}
