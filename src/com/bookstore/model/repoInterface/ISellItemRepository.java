package com.bookstore.model.repoInterface;

import com.bookstore.model.entity.SellItem;

import java.util.List;

public interface ISellItemRepository {
    SellItem create(SellItem sellItem);
    SellItem update(SellItem sellItem);
    Boolean delete(Long id);
    SellItem findById(Long id);
    List<SellItem> findAll();
    List<SellItem> findAllBySellId(Long id);

}
