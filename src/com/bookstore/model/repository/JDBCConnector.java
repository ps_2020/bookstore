package com.bookstore.model.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnector {
    private static final String DB_URL = "jdbc:mysql://localhost:3307/";
    private static final String USER = "root";
    private static final String PASS = "database";
    private static final int TIMEOUT = 5;

    private Connection connection;

    public JDBCConnector(String schemaName) {
        try {
            connection = DriverManager.getConnection(DB_URL + schemaName + "?allowPublicKeyRetrieval=true&useSSL=false", USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean testConnection() throws SQLException {
        return connection.isValid(TIMEOUT);
    }

    public Connection getConnection() {
        return connection;
    }


}
