package com.bookstore.model.repository;

import com.bookstore.model.entity.Book;
import com.bookstore.model.repoInterface.IBookRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookRepository implements IBookRepository {

    private final JDBCConnector jdbcConnector;

    public BookRepository(JDBCConnector jdbcConnector) {
        this.jdbcConnector = jdbcConnector;
    }

    @Override
    public Book create(Book book) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO book (title, genre, price, author, quantity) VALUES(?, ?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getGenre());
            preparedStatement.setDouble(3, book.getPrice());
            preparedStatement.setString(4, book.getAuthor());
            preparedStatement.setInt(5, book.getQuantity());


            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()) {
                book.setBookId(resultSet.getLong(1));
                return book;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Book update(Book book) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE book SET title=?, genre=?, price=?, author=?, quantity=? WHERE idBook=?",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getGenre());
            preparedStatement.setDouble(3, book.getPrice());
            preparedStatement.setString(4, book.getAuthor());
            preparedStatement.setInt(5, book.getQuantity());
            preparedStatement.setLong(6, book.getBookId());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0) {
                return book;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM book WHERE idBook=?");
            preparedStatement.setLong(1, id);

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Book findById(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE idBook=?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Book book = new Book();

                book.setBookId(resultSet.getLong(1));
                book.setTitle(resultSet.getString(2));
                book.setGenre(resultSet.getString(3));
                book.setPrice(resultSet.getDouble(4));
                book.setAuthor(resultSet.getString(5));
                book.setQuantity(resultSet.getInt(6));

                return book;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Book> findAll() {
        Connection connection = jdbcConnector.getConnection();
        List<Book> books = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();

                book.setBookId(resultSet.getLong(1));
                book.setTitle(resultSet.getString(2));
                book.setGenre(resultSet.getString(3));
                book.setPrice(resultSet.getDouble(4));
                book.setAuthor(resultSet.getString(5));
                book.setQuantity(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public List<Book> search(String searchString){
        Connection connection = jdbcConnector.getConnection();
        List<Book> books = new ArrayList<>();
        searchString = "%" + searchString + "%";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM book WHERE title LIKE ? OR author LIKE ? OR genre LIKE ?");
            preparedStatement.setString(1, searchString);
            preparedStatement.setString(2, searchString);
            preparedStatement.setString(3, searchString);
            ResultSet resultSet = preparedStatement.executeQuery();


            while (resultSet.next()) {
                Book book = new Book();
                book.setBookId(resultSet.getLong(1));
                book.setTitle(resultSet.getString(2));
                book.setGenre(resultSet.getString(3));
                book.setPrice(resultSet.getDouble(4));
                book.setAuthor(resultSet.getString(5));
                book.setQuantity(resultSet.getInt(6));

                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return books;
    }
}
