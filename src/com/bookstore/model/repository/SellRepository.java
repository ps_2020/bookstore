package com.bookstore.model.repository;

import com.bookstore.model.entity.Sell;
import com.bookstore.model.repoInterface.ISellRepository;
import java.util.Date;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SellRepository implements ISellRepository {

    private final JDBCConnector jdbcConnector;

    public SellRepository(JDBCConnector jdbcConnector) {
        this.jdbcConnector = jdbcConnector;
    }

    @Override
    public Sell create(Sell sell) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sells (date, totalPrice, userId) VALUES(?, ?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDate(1, new java.sql.Date(sell.getDate().getTime()));
            preparedStatement.setDouble(2, sell.getTotalPrice());
            preparedStatement.setLong(3,sell.getUserId());

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                sell.setSellId(resultSet.getLong(1));
                return sell;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Sell update(Sell sell) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sells SET date=?, totalPrice=?, userId=? WHERE idSell=?",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDate(1, new java.sql.Date(sell.getDate().getTime()));
            preparedStatement.setDouble(2, sell.getTotalPrice());
            preparedStatement.setLong(3,sell.getUserId());
            preparedStatement.setLong(4, sell.getSellId());


            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0) {
                return sell;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sells WHERE idSell=?");
            preparedStatement.setLong(1, id);

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public Sell findById(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sells WHERE idSell=?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Sell sell = new Sell();

                sell.setSellId(resultSet.getLong(1));
                sell.setDate(resultSet.getDate(2));
                sell.setTotalPrice(resultSet.getDouble(3));
                sell.setUserId(resultSet.getLong(4));

                return sell;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public List<Sell> findAll() {
        Connection connection = jdbcConnector.getConnection();
        List<Sell> sellList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sells");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Sell sell = new Sell();

                sell.setSellId(resultSet.getLong(1));
                sell.setDate(resultSet.getDate(2));
                sell.setTotalPrice(resultSet.getDouble(3));
                sell.setUserId(resultSet.getLong(4));

                sellList.add(sell);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sellList;
    }

    @Override
    public List<Sell> findAllByUserId(Long id) {
        Connection connection = jdbcConnector.getConnection();
        List<Sell> sellList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sells where userId = ?");
            preparedStatement.setLong(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Sell sell = new Sell();

                sell.setSellId(resultSet.getLong(1));
                sell.setDate(resultSet.getDate(2));
                sell.setTotalPrice(resultSet.getDouble(3));
                sell.setUserId(resultSet.getLong(4));

                sellList.add(sell);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sellList;
    }
}

