package com.bookstore.model.repository;

import com.bookstore.model.entity.SellItem;
import com.bookstore.model.repoInterface.ISellItemRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SellItemRepository implements ISellItemRepository {

    private final JDBCConnector jdbcConnector;

    public SellItemRepository(JDBCConnector jdbcConnector) {
        this.jdbcConnector = jdbcConnector;
    }

    @Override
    public SellItem create(SellItem sellItem) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sell_items (sellId, bookId, quantity) VALUES(?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, sellItem.getSellId());
            preparedStatement.setLong(2, sellItem.getBookId());
            preparedStatement.setInt(3, sellItem.getQuantity());


            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()) {
                sellItem.setSellItemId(resultSet.getLong(1));
                return sellItem;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SellItem update(SellItem sellItem) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sell_items SET sellId=?, bookId=?,quantity=? WHERE idSellItems=?",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, sellItem.getSellId());
            preparedStatement.setLong(2, sellItem.getBookId());
            preparedStatement.setInt(3, sellItem.getQuantity());
            preparedStatement.setLong(4, sellItem.getSellItemId());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0) {
                return sellItem;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sell_items WHERE idSellItems=?");
            preparedStatement.setLong(1, id);

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public SellItem findById(Long id) {
        Connection connection = jdbcConnector.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sell_items WHERE idSellItems=?");
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                SellItem sellItem = new SellItem();

                sellItem.setSellItemId(resultSet.getLong(1));
                sellItem.setSellId(resultSet.getLong(2));
                sellItem.setBookId(resultSet.getLong(3));
                sellItem.setQuantity(resultSet.getInt(4));

                return sellItem;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<SellItem> findAll() {
        Connection connection = jdbcConnector.getConnection();
        List<SellItem> sellItems = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sell_items");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                SellItem sellItem = new SellItem();

                sellItem.setSellItemId(resultSet.getLong(1));
                sellItem.setSellId(resultSet.getLong(2));
                sellItem.setBookId(resultSet.getLong(3));
                sellItem.setQuantity(resultSet.getInt(4));

                sellItems.add(sellItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sellItems;
    }

    @Override
    public List<SellItem> findAllBySellId(Long id) {
        Connection connection = jdbcConnector.getConnection();
        List<SellItem> sellItems = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM sell_items where sellId=?");
            preparedStatement.setLong(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                SellItem sellItem = new SellItem();

                sellItem.setSellItemId(resultSet.getLong(1));
                sellItem.setSellId(resultSet.getLong(2));
                sellItem.setBookId(resultSet.getLong(3));
                sellItem.setQuantity(resultSet.getInt(4));

                sellItems.add(sellItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sellItems;
    }


}
