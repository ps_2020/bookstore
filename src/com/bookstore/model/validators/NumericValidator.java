package com.bookstore.model.validators;

import com.bookstore.model.validatorsInterface.INumericValidator;

public class NumericValidator implements INumericValidator<String> {

    @Override
    public boolean validateDouble(String string) {
        try {
            Double doubleString = Double.parseDouble(string);
        } catch (Exception e1) {
            System.out.println("Expected numeric value: Double");
            return false;
        }
        return true;
    }
    @Override
    public boolean validateInt(String string) {
        try {
            int intString = Integer.parseInt(string);
        } catch (Exception e1) {
            System.out.println("Expected numeric value: Int");
            return false;
        }
        return true;
    }


}
