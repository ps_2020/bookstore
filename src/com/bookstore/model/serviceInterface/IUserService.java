package com.bookstore.model.serviceInterface;

import com.bookstore.model.entity.User;

import java.util.List;

public interface IUserService {
    User create(User User);
    User update(User User);
    Boolean delete(Long id);
    User findById(Long id);
    List<User> findAll();
    User login(String username, String password);

}
