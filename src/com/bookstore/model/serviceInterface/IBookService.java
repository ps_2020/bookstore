package com.bookstore.model.serviceInterface;

import com.bookstore.model.entity.Book;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

public interface IBookService {
    Book save(Book book);
    Boolean delete(Long id);
    List<Book> findAll();
    List<Book> search(String searchString);

    Book findById(Long id);
}
