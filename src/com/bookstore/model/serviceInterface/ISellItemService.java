package com.bookstore.model.serviceInterface;

import com.bookstore.model.dataTransfer.SellDTO;
import com.bookstore.model.entity.Sell;
import com.bookstore.model.entity.SellItem;

import java.util.List;

public interface ISellItemService {
    SellItem saveItem(SellItem sellItem);
    Boolean deleteItem(Long id);
    SellItem findItemById(Long id);
    List<SellItem> findAllItems();

    Sell saveSell(Sell sell);
    Boolean deleteSell(Long id);
    Sell findSellById(Long id);
    List<Sell> findAllSells();

    Sell sellBooks(SellDTO sellDTO);
}
