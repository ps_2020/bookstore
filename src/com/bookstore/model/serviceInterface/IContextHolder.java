package com.bookstore.model.serviceInterface;

import com.bookstore.model.entity.User;

public interface IContextHolder {

    void setCurrentUser(User user);
    User getCurrentUser();
}
