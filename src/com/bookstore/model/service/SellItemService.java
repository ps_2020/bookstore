package com.bookstore.model.service;

import com.bookstore.model.dataTransfer.SellDTO;
import com.bookstore.model.dataTransfer.SellItemDTO;
import com.bookstore.model.entity.Book;
import com.bookstore.model.entity.Sell;
import com.bookstore.model.entity.SellItem;
import com.bookstore.model.entity.User;
import com.bookstore.model.repoInterface.IBookRepository;
import com.bookstore.model.repoInterface.ISellItemRepository;
import com.bookstore.model.repoInterface.ISellRepository;
import com.bookstore.model.repoInterface.IUserRepository;
import com.bookstore.model.serviceInterface.ISellItemService;

import java.util.ArrayList;
import java.util.List;

public class SellItemService implements ISellItemService {

    private final ISellItemRepository sellItemRepository;
    private final ISellRepository sellRepository;
    private final IUserRepository userRepository;
    private final IBookRepository bookRepository;

    public SellItemService(ISellItemRepository sellItemRepository, ISellRepository sellRepository, IUserRepository userRepository, IBookRepository bookRepository) {
        this.sellItemRepository = sellItemRepository;
        this.sellRepository = sellRepository;
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public SellItem saveItem(SellItem sellItem){
        if(sellItem.getSellItemId() != null){
            return sellItemRepository.update(sellItem);
        }
        else{
            return sellItemRepository.create(sellItem);
        }
    }

    @Override
    public Boolean deleteItem(Long id){
        return sellItemRepository.delete(id);
    }

    @Override
    public SellItem findItemById(Long id){
        return sellItemRepository.findById(id);
    }

    @Override
    public List<SellItem> findAllItems() {
        return sellItemRepository.findAll();
    }

    @Override
    public Sell saveSell(Sell sell) {
        if(sell.getSellId() != null){
            return sellRepository.update(sell);
        }
        else{
            return sellRepository.create(sell);
        }
    }

    @Override
    public Boolean deleteSell(Long id) {
        return sellRepository.delete(id);
    }

    @Override
    public Sell findSellById(Long id) {
        return sellRepository.findById(id);
    }

    @Override
    public List<Sell> findAllSells() {
        return sellRepository.findAll();
    }


    @Override
    public Sell sellBooks(SellDTO sellDTO) {

        Sell sell = new Sell();
        sell.setDate(sellDTO.getDate());
        sell.setTotalPrice(sellDTO.getTotalPrice());
        sell.setUserId(sellDTO.getUser().getUserId());

        sell = sellRepository.create(sell);

        if(sell != null){
            for(SellItemDTO sid:sellDTO.getSellItems()){
                SellItem sellItem = new SellItem();

                sellItem.setBookId(sid.getBook().getBookId());
                sellItem.setQuantity(sid.getQuantity());
                sellItem.setSellId(sell.getSellId());

                sellItem = sellItemRepository.create(sellItem);

                Book book = sid.getBook();
                book.setQuantity(book.getQuantity() - sid.getQuantity());
                bookRepository.update(book);
                if(sellItem == null)
                    return null;
            }
            return sell;
        }


        return null;
    }


}
