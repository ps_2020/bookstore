package com.bookstore.model.service;

import com.bookstore.model.entity.User;
import com.bookstore.model.serviceInterface.IContextHolder;

public class ContextHolder implements IContextHolder {

    private User currentUser;

    @Override
    public void setCurrentUser(User user) {
        this.currentUser = user;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }
}
