package com.bookstore.model.service;

import com.bookstore.model.dataTransfer.SellDTO;
import com.bookstore.model.dataTransfer.SellItemDTO;
import com.bookstore.model.entity.Book;
import com.bookstore.model.entity.Sell;
import com.bookstore.model.entity.SellItem;
import com.bookstore.model.entity.User;
import com.bookstore.model.repoInterface.IBookRepository;
import com.bookstore.model.repoInterface.ISellItemRepository;
import com.bookstore.model.repoInterface.ISellRepository;
import com.bookstore.model.repoInterface.IUserRepository;
import com.bookstore.model.serviceInterface.IUserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

public class UserService  implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;

    }

    @Override
    public User create(User user) {
        return userRepository.create(user);
    }

    @Override
    public User update(User user) {
        if(userRepository.findById(user.getUserId()) != null){
            return userRepository.update(user);
        }
        else{
            return userRepository.create(user);
        }
    }

    @Override
    public Boolean delete(Long id) {
        return userRepository.delete(id);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    @Override
    public User login(String username, String password) {
        User user = userRepository.findByUsername(username);
        if(user != null) {
            if(Objects.equals(user.getPassword(), password)) {

                System.out.println("User found!");
               return user;
            } else {
                LOGGER.warning("Wrong password for user " + username);
            }
        } else {
            LOGGER.warning("User with username: " + username + " was not found");
        }
        return null;
    }

}
