package com.bookstore.model.service;

import com.bookstore.model.entity.Book;
import com.bookstore.model.repoInterface.IBookRepository;
import com.bookstore.model.repository.BookRepository;
import com.bookstore.model.serviceInterface.IBookService;

import java.util.List;

public class BookService implements IBookService {
    private final IBookRepository bookRepository;

    public BookService(IBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book save(Book book){
        if(bookRepository.findById(book.getBookId()) != null){
            return bookRepository.update(book);
        }
        else{
            return bookRepository.create(book);
        }
    }

    @Override
    public Boolean delete(Long id){
        return bookRepository.delete(id);
    }

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> search(String searchString){
        return bookRepository.search(searchString);
    }

    @Override
    public Book findById(Long id){return bookRepository.findById(id);};


}
