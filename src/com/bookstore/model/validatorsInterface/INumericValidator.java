package com.bookstore.model.validatorsInterface;

public interface INumericValidator<T> {

    public boolean validateDouble(T string);
    public boolean validateInt(T string);
}
