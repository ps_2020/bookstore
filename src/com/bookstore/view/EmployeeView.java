package com.bookstore.view;

import com.bookstore.controller.EmployeeController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class EmployeeView extends JFrame {

    public JPanel contentPane;
    public JTable tblBooks;
    public JTextField txtSearch;
    public JTable tblSells;
    public JTextField txtQuantity;
    public JTextField txtTotal;
    public JButton btnSearch;
    public JButton btnSell;
    private JButton btnAddBook;

    /**
     * Create the frame.
     */
    public EmployeeView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 688, 495);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 47, 652, 207);
        contentPane.add(scrollPane);

        tblBooks = new JTable();
        scrollPane.setViewportView(tblBooks);

        txtSearch = new JTextField();
        txtSearch.setBounds(88, 12, 433, 20);
        contentPane.add(txtSearch);
        txtSearch.setColumns(10);

        JLabel lblSearch = new JLabel("Search");
        lblSearch.setFont(new Font("Tahoma", Font.PLAIN, 13));
        lblSearch.setHorizontalAlignment(SwingConstants.RIGHT);
        lblSearch.setBounds(10, 11, 68, 20);
        contentPane.add(lblSearch);

        btnSearch = new JButton("Search");
        btnSearch.setBounds(531, 11, 89, 23);
        contentPane.add(btnSearch);

        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(10, 268, 319, 147);
        contentPane.add(scrollPane_1);

        tblSells = new JTable();
        scrollPane_1.setViewportView(tblSells);

        txtQuantity = new JTextField();
        txtQuantity.setBounds(352, 292, 101, 20);
        contentPane.add(txtQuantity);
        txtQuantity.setColumns(10);

        JLabel lbQuantity = new JLabel("Quantity");
        lbQuantity.setFont(new Font("Tahoma", Font.PLAIN, 13));
        lbQuantity.setHorizontalAlignment(SwingConstants.CENTER);
        lbQuantity.setBounds(352, 267, 101, 20);
        contentPane.add(lbQuantity);

        btnSell = new JButton("SELL");
        btnSell.setBounds(352, 364, 101, 52);
        contentPane.add(btnSell);

        txtTotal = new JTextField();
        txtTotal.setEditable(false);
        txtTotal.setBounds(262, 425, 68, 20);
        contentPane.add(txtTotal);
        txtTotal.setColumns(10);

        JLabel lblTotal = new JLabel("Total");
        lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 13));
        lblTotal.setHorizontalAlignment(SwingConstants.RIGHT);
        lblTotal.setBounds(206, 426, 46, 17);
        contentPane.add(lblTotal);

        btnAddBook = new JButton("Add book");
        btnAddBook.setBounds(352, 323, 101, 23);
        contentPane.add(btnAddBook);
    }

    public void refreshBooksTable(DefaultTableModel model) {
        tblBooks.setModel(model);
    }

    public void refreshSellsTable(DefaultTableModel model) {
        tblSells.setModel(model);
    }

    public void addSearchBookActionListener(ActionListener searchBookActionListener) {
        this.btnSearch.addActionListener(searchBookActionListener);
    }

    public void addAddBookToSellActionListener(ActionListener addBookToSellActionListener) {
        this.btnAddBook.addActionListener(addBookToSellActionListener);
    }

    public void addSellBooksActionListener(ActionListener sellBooksActionListener) {
        this.btnSell.addActionListener(sellBooksActionListener);
    }

    public String getSearchTxt() {
        return this.txtSearch.getText();
    }

    public JTable getTblBooks() {
        return this.tblBooks;
    }

    public int getQuantity() {
        return Integer.parseInt(this.txtQuantity.getText());
    }

    public void setTotalPrice(double totalPrice) {
        this.txtTotal.setText("" + totalPrice);
    }
}
