package com.bookstore.view;

import com.bookstore.controller.LoginController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LoginView extends JFrame {

    private JPasswordField txtPassword;
    private JTextField txtUsername;
    private JButton btnLogin;

    /**
     * Create the application.
     */
    public LoginView() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        this.setBounds(100, 100, 450, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        JLabel lblNewLabel_1 = new JLabel("Username");
        lblNewLabel_1.setHorizontalAlignment(SwingConstants.TRAILING);
        lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
        lblNewLabel_1.setBounds(91, 93, 77, 20);
        this.getContentPane().add(lblNewLabel_1);

        txtPassword = new JPasswordField();
        txtPassword.setBounds(178, 118, 113, 20);
        this.getContentPane().add(txtPassword);
        txtPassword.setColumns(10);

        txtUsername = new JTextField();
        txtUsername.setBounds(178, 93, 113, 20);
        this.getContentPane().add(txtUsername);
        txtUsername.setColumns(10);

        JLabel lblNewLabel = new JLabel("Password");
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
        lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        lblNewLabel.setBounds(101, 117, 67, 20);
        this.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel_2 = new JLabel("LOGIN");
        lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel_2.setBounds(156, 11, 113, 35);
        this.getContentPane().add(lblNewLabel_2);

        btnLogin = new JButton("Login");
        btnLogin.setBounds(178, 163, 113, 23);
        this.getContentPane().add(btnLogin);
    }

    public String getUsernameTxt() {
        return this.txtUsername.getText();
    }

    public String getPasswordTxt(){
        return this.txtPassword.getText();
    }

    public void addLoginActionListener(ActionListener loginActionListener) {
        this.btnLogin.addActionListener(loginActionListener);
    }
}
