package com.bookstore.view;

import com.bookstore.controller.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

public class AdminView extends JFrame {

    private JPanel contentPane;

    public JTable getTblAdmin() {
        return tblAdmin;
    }

    private JTable tblAdmin;
    private JButton btnSave;
    private JButton btnDelete;
    private JButton btnGenReports;
    private JRadioButton rdbtnUsers;
    private JRadioButton rdbtnBooks;

    private String currentBtn = "Users";

    /**
     * Create the frame.
     */
    public AdminView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 657, 504);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 47, 652, 200);
        contentPane.add(scrollPane);

        tblAdmin = new JTable();
        scrollPane.setViewportView(tblAdmin);

        btnSave = new JButton("Save");
        btnSave.setBounds(156, 314, 117, 43);
        contentPane.add(btnSave);

        btnDelete = new JButton("Delete");
        btnDelete.setBounds(362, 314, 117, 43);
        contentPane.add(btnDelete);

        btnGenReports = new JButton("GENERATE REPORTS");
        btnGenReports.setBounds(156, 383, 323, 43);
        contentPane.add(btnGenReports);

        rdbtnUsers = new JRadioButton("Users");
        rdbtnUsers.setFont(new Font("Tahoma", Font.PLAIN, 13));
        rdbtnUsers.setBounds(291, 248, 73, 23);
        contentPane.add(rdbtnUsers);

        rdbtnBooks = new JRadioButton("Books");
        rdbtnBooks.setFont(new Font("Tahoma", Font.PLAIN, 13));
        rdbtnBooks.setBounds(291, 275, 73, 23);
        contentPane.add(rdbtnBooks);
    }


    public String getCurrentBtn() {
        return currentBtn;
    }

    public void setCurrentBtn(String currentBtn) {
        this.currentBtn = currentBtn;
    }

    public void addSaveActionListener(ActionListener saveActionListener) {
        this.btnSave.addActionListener(saveActionListener);
    }

    public void addDeleteActionListener(ActionListener deleteActionListener) {
        this.btnDelete.addActionListener(deleteActionListener);
    }

    public void addSwitchUsersToBooksActionlistener(ActionListener switchUserToBooksActionListener) {
        this.rdbtnBooks.addActionListener(switchUserToBooksActionListener);
    }

    public void addSwitchBooksToUserssActionlistener(ActionListener switchBooksToUsersActionListener) {
        this.rdbtnUsers.addActionListener(switchBooksToUsersActionListener);
    }

    public void switchRdbtnState(){
        if(getCurrentBtn().contentEquals("Users")){
           rdbtnBooks.setSelected(false);
           rdbtnBooks.setContentAreaFilled(false);
        }
        else if(getCurrentBtn().contentEquals("Books")){
            rdbtnUsers.setSelected(false);
            rdbtnUsers.setContentAreaFilled(false);
        }
    }

    public void refreshTable(DefaultTableModel model) {
        tblAdmin.setModel(model);
    }

    public void addGenerateReportsActionListener(ActionListener generateReportsActionListener) {
        btnGenReports.addActionListener(generateReportsActionListener);
    }
}
