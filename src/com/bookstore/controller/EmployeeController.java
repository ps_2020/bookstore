package com.bookstore.controller;

import com.bookstore.model.dataTransfer.SellDTO;
import com.bookstore.model.dataTransfer.SellItemDTO;
import com.bookstore.model.entity.Book;
import com.bookstore.model.entity.User;
import com.bookstore.model.serviceInterface.IBookService;
import com.bookstore.model.serviceInterface.IContextHolder;
import com.bookstore.model.serviceInterface.ISellItemService;
import com.bookstore.model.validators.SellValidator;
import com.bookstore.view.EmployeeView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeeController {

    private final ISellItemService sellItemService;
    private final IBookService bookService;
    private final EmployeeView employeeView;
    private final IContextHolder contextHolder;
    private SellDTO sellDTO;

    public EmployeeController(ISellItemService sellItemService, IBookService bookService, EmployeeView employeeView, IContextHolder contextHolder) {
        this.sellItemService = sellItemService;
        this.bookService = bookService;
        this.employeeView = employeeView;
        this.contextHolder = contextHolder;

        this.sellDTO = new SellDTO();

        // get initial view data
        refreshTable("Books");
        refreshTable("Sells");

        // set Action Listeners
        this.employeeView.addSearchBookActionListener(new SearchBookActionListener());
        this.employeeView.addAddBookToSellActionListener(new AddBookToSellActionListener());
        this.employeeView.addSellBooksActionListener(new SellBooksActionListener());

    }

    // private classes for Action Listeners

    private class SearchBookActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final List<Book> books = bookService.search(employeeView.getSearchTxt());
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("Book Id");
            model.addColumn("Title");
            model.addColumn("Author");
            model.addColumn("Genre");
            model.addColumn("Price");
            model.addColumn("Quantity");

            for (Book b : books) {
                if (b != null) {
                    Object[] o = {b.getBookId(), b.getTitle(), b.getAuthor(), b.getGenre(), b.getPrice(), b.getQuantity()};
                    model.addRow(o);
                }
            }
            employeeView.refreshBooksTable(model);
        }
    }

    private class AddBookToSellActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            SellValidator sellValidator = new SellValidator();
            SellItemDTO sellItemDTO = new SellItemDTO();

            JTable tblBooks = employeeView.getTblBooks();
            Book book = bookService.findById(Long.parseLong(tblBooks.getValueAt(tblBooks.getSelectedRow(),0).toString()));
            sellItemDTO.setQuantity(employeeView.getQuantity());
            sellItemDTO.setBook(book);


            if(sellValidator.validateQuantity(sellItemDTO.getQuantity(), book.getQuantity())) {
                tblBooks.setValueAt(Integer.parseInt(tblBooks.getValueAt(tblBooks.getSelectedRow(),5).toString()) - sellItemDTO.getQuantity(), tblBooks.getSelectedRow(),5);
                sellDTO.addSellItem(sellItemDTO);
            }
            else{
                System.out.println("Not enough books in stock");
            }
            refreshTable("Sells");
        }
    }

    private class SellBooksActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            sellDTO.setUser(contextHolder.getCurrentUser());
            sellDTO.setDate(new Date());
            if(sellItemService.sellBooks(sellDTO) != null)
                System.out.println("Sell successfully!");
            else
                System.out.println("Something went wrong");


            sellDTO = new SellDTO();
            refreshTable("Sells");
            refreshTable("Books");
        }
    }

    private void refreshTable(String table) {
        if(table.contentEquals("Sells")){

            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("Book Title");
            model.addColumn("Quantity");

            if(sellDTO != null){
                if(sellDTO.getSellItems() != null)
                    for(SellItemDTO sid: sellDTO.getSellItems()){
                        if(sid != null) {
                            Object[] o = {sid.getBook().getTitle(), sid.getQuantity()};
                            model.addRow(o);
                        }
                    }
            }
            employeeView.setTotalPrice(sellDTO.getTotalPrice());
            employeeView.refreshSellsTable(model);
        }
        else if(table.contentEquals("Books")) {
            final List<Book> books = bookService.findAll();
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("Book Id");
            model.addColumn("Title");
            model.addColumn("Author");
            model.addColumn("Genre");
            model.addColumn("Price");
            model.addColumn("Quantity");

            for (Book b : books) {
                if (b != null) {
                    Object[] o = {b.getBookId(), b.getTitle(), b.getAuthor(), b.getGenre(), b.getPrice(), b.getQuantity()};
                    model.addRow(o);
                }
            }
            employeeView.refreshBooksTable(model);
        }
    }
}
