package com.bookstore.controller;

import com.bookstore.factory.ReportFactory;
import com.bookstore.factory.ReportI;
import com.bookstore.factory.ReportTxt;
import com.bookstore.model.dataTransfer.SellDTO;
import com.bookstore.model.entity.Book;
import com.bookstore.model.entity.User;
import com.bookstore.model.serviceInterface.IBookService;
import com.bookstore.model.serviceInterface.IContextHolder;
import com.bookstore.model.serviceInterface.IUserService;
import com.bookstore.model.validators.NumericValidator;
import com.bookstore.view.AdminView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AdminController {

    private final IUserService userService;
    private final IBookService bookService;
    private final AdminView adminView;
    private final IContextHolder contextHolder;
    private SellDTO sellDTO;


    public AdminController(IUserService userService, IBookService bookService, AdminView adminView, IContextHolder contextHolder) {
        this.userService = userService;
        this.bookService = bookService;
        this.adminView = adminView;
        this.contextHolder = contextHolder;

        this.sellDTO = new SellDTO();

        // get initial view data
        refreshTable("Users");


        // set Action Listeners
        this.adminView.addSaveActionListener(new SaveActionListener());
        this.adminView.addDeleteActionListener(new DeleteActionListener());

        this.adminView.addSwitchUsersToBooksActionlistener(new SwitchUsersToBooksActionListener());
        this.adminView.addSwitchBooksToUserssActionlistener(new SwitchBooksToUsersActionListener());

        this.adminView.addGenerateReportsActionListener(new GenerateReportsActionListener());
    }

    // private classes for Action Listeners
    private class SaveActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            NumericValidator numericValidator = new NumericValidator();
            if(adminView.getCurrentBtn().contentEquals("Users")){
                User user = new User();
                JTable tblAdmin = adminView.getTblAdmin();

                user.setUserId(Long.parseLong(tblAdmin.getValueAt(tblAdmin.getSelectedRow(),0).toString()));
                user.setUsername(tblAdmin.getValueAt(tblAdmin.getSelectedRow(),1).toString());
                user.setPassword(tblAdmin.getValueAt(tblAdmin.getSelectedRow(),2).toString());
                user.setRole(tblAdmin.getValueAt(tblAdmin.getSelectedRow(),3).toString());

                userService.update(user);
                refreshTable("Users");
            }
            else if(adminView.getCurrentBtn().contentEquals("Books")){

                Book book = new Book();
                JTable tblAdmin = adminView.getTblAdmin();

                if((numericValidator.validateInt(tblAdmin.getValueAt(tblAdmin.getSelectedRow(),5).toString()))
                    && (numericValidator.validateDouble((tblAdmin.getValueAt(tblAdmin.getSelectedRow(),4).toString())))) {
                    book.setQuantity(Integer.parseInt(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 5).toString()));
                    book.setAuthor(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 2).toString());
                    book.setPrice(Double.parseDouble(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 4).toString()));
                    book.setGenre(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 3).toString());
                    book.setTitle(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 1).toString());
                    book.setBookId(Long.parseLong(tblAdmin.getValueAt(tblAdmin.getSelectedRow(), 0).toString()));

                    bookService.save(book);
                }
                refreshTable("Books");
            }
        }
    }

    private class DeleteActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(adminView.getCurrentBtn().contentEquals("Users")){
                JTable tblAdmin = adminView.getTblAdmin();

                userService.delete(new Long((long)tblAdmin.getValueAt(tblAdmin.getSelectedRow(),0)));
                refreshTable("Users");
            }
            else if(adminView.getCurrentBtn().contentEquals("Books")){

                JTable tblAdmin = adminView.getTblAdmin();
                bookService.delete(new Long((long)tblAdmin.getValueAt(tblAdmin.getSelectedRow(),0)));
                refreshTable("Books");
            }
        }
    }

    private class SwitchUsersToBooksActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            adminView.setCurrentBtn("Books");
            System.out.println("Books clicked");

            adminView.switchRdbtnState();

            refreshTable("Books");
        }
    }

    private class SwitchBooksToUsersActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            adminView.setCurrentBtn("Users");
            System.out.println("Users clicked");

            adminView.switchRdbtnState();

            refreshTable("Users");
        }
    }

    private class GenerateReportsActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            ReportFactory reportFactory = new ReportFactory();
            final List<Book> books = bookService.findAll();

            Collection<Book> filteredCollection = books
                    .stream()
                    .filter(b -> b.getQuantity() == 0)
                    .collect(Collectors.toList());

            ReportI reportTxt = reportFactory.getReport("TXT");
            reportTxt.GenerateReport((List<Book>) filteredCollection, new Date());

            ReportI reportCsv = reportFactory.getReport("CSV");
            reportCsv.GenerateReport((List<Book>) filteredCollection, new Date());

        }
    }
    private void refreshTable(String table){
        if(table.contentEquals("Users")){
            final List<User> users = userService.findAll();
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("User Id");
            model.addColumn("Username");
            model.addColumn("Password");
            model.addColumn("Role");

            for(User u: users){
                if(u != null) {
                    Object[] o = {u.getUserId(), u.getUsername(), u.getPassword(), u.getRole()};
                    model.addRow(o);
                }
            }
            model.addRow(new Object[6]);
            adminView.refreshTable(model);
        }
        else if(table.contentEquals("Books")){
            final List<Book> books = bookService.findAll();
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("Book Id");
            model.addColumn("Title");
            model.addColumn("Author");
            model.addColumn("Genre");
            model.addColumn("Price");
            model.addColumn("Quantity");

            for(Book b: books){
                if(b != null) {
                    Object[] o = {b.getBookId(), b.getTitle(), b.getAuthor(), b.getGenre(), b.getPrice(), b.getQuantity()};
                    model.addRow(o);
                }
            }
            model.addRow(new Object[6]);
            adminView.refreshTable(model);
        }
    }
}
