package com.bookstore.controller;

import com.bookstore.Bookstore;
import com.bookstore.model.entity.User;
import com.bookstore.model.service.UserService;
import com.bookstore.model.serviceInterface.IContextHolder;
import com.bookstore.model.serviceInterface.IUserService;
import com.bookstore.view.LoginView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginController {

    private final IUserService userService;
    private final LoginView loginView;
    private final IContextHolder contextHolder;

    public LoginController(IUserService userService, LoginView loginView, IContextHolder contextHolder) {
        this.userService = userService;
        this.loginView = loginView;
        this.contextHolder = contextHolder;

        // set Action Listeners
        this.loginView.addLoginActionListener(new LoginActionListener());
    }

    // private classes for Action Listeners

    private class LoginActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String username = loginView.getUsernameTxt();
            String password = loginView.getPasswordTxt();

            User user = userService.login(username,password);
            if(user != null){
                if(user.getRole().contentEquals("admin")){
                   Bookstore.openAdminView();
                }
                else if(user.getRole().contentEquals("employee")){
                   Bookstore.openEmployeeView();
                }
            }
            contextHolder.setCurrentUser(user);

        }
    }

}
