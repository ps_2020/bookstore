package com.bookstore.factory;

import com.bookstore.model.entity.Book;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ReportCsv implements ReportI {

    @Override
    public void GenerateReport(List<Book> bookList, Date date) {
        for (Book book : bookList) {
            try {
                //   if(book.getQuantity() == 0) {
                System.out.println(book.toString());
                writeToFile(book, "src/com/bookstore/CSVReport" + "-" + date.getHours() + "-" + date.getMinutes() + "-" + date.getSeconds() + ".csv");
                //   }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private static void writeToFile(Book book, String fileName) throws FileNotFoundException, IOException{
        File file = new File(fileName);
        String content = book.toCSV();

        try (FileOutputStream fop = new FileOutputStream(file, true)) {
            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            //fop.flush();
        }
    }
}
