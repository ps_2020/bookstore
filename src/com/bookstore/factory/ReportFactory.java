package com.bookstore.factory;

public class ReportFactory {

    public ReportI getReport(String reportType) {

        switch(reportType) {
            case "TXT":
                return new ReportTxt();
            case "CSV":
                return new ReportCsv();
        }
        return null;
    }
}
