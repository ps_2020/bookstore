package com.bookstore;

import com.bookstore.controller.AdminController;
import com.bookstore.controller.EmployeeController;
import com.bookstore.controller.LoginController;
import com.bookstore.model.entity.Book;
import com.bookstore.model.entity.User;
import com.bookstore.model.repoInterface.IBookRepository;
import com.bookstore.model.repoInterface.ISellItemRepository;
import com.bookstore.model.repoInterface.ISellRepository;
import com.bookstore.model.repoInterface.IUserRepository;
import com.bookstore.model.repository.*;
import com.bookstore.model.service.BookService;
import com.bookstore.model.service.ContextHolder;
import com.bookstore.model.service.SellItemService;
import com.bookstore.model.service.UserService;
import com.bookstore.model.serviceInterface.IBookService;
import com.bookstore.model.serviceInterface.IContextHolder;
import com.bookstore.model.serviceInterface.ISellItemService;
import com.bookstore.model.serviceInterface.IUserService;
import com.bookstore.view.AdminView;
import com.bookstore.view.EmployeeView;
import com.bookstore.view.LoginView;


public class Bookstore {
    private static JDBCConnector jdbcConnector;
    private static IUserRepository userRepository;
    private static IBookRepository bookRepository;
    private static ISellItemRepository sellItemRepository;
    private static ISellRepository sellRepository;

    private static IUserService userService;
    private static IBookService bookService;
    private static ISellItemService sellItemService;
    private static IContextHolder contextHolder;

    private static EmployeeView employeeView;
    private static AdminView adminView;
    private static LoginView loginView;

    private static EmployeeController employeeController;
    private static LoginController loginController;
    private static AdminController adminController;

    public static void main(String[] args) {

        jdbcConnector = new JDBCConnector("bookstore");

        userRepository = new UserRepository(jdbcConnector);
        bookRepository = new BookRepository(jdbcConnector);
        sellRepository = new SellRepository(jdbcConnector);
        sellItemRepository = new SellItemRepository(jdbcConnector);

        userService = new UserService(userRepository);
        bookService = new BookService(bookRepository);
        sellItemService = new SellItemService(sellItemRepository, sellRepository, userRepository, bookRepository);
        contextHolder = new ContextHolder();

        loginView = new LoginView();
        loginController = new LoginController(userService, loginView, contextHolder);

        loginView.setVisible(true);
    }

    public static void openEmployeeView() {
        employeeView = new EmployeeView();
        employeeController = new EmployeeController(sellItemService, bookService,employeeView, contextHolder);

        loginView.setVisible(false);
        employeeView.setVisible(true);
    }
    public static void openAdminView() {
        adminView = new AdminView();
        adminController = new AdminController(userService, bookService,adminView, contextHolder);
        loginView.setVisible(false);
        adminView.setVisible(true);
    }
}

